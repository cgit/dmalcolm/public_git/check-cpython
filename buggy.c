/* 
   Errroneus argument parsing of socket.htons() on 64bit big endian
   machines.

   Fixed in svn r34931
*/

static PyObject *
socket_htons(PyObject *self, PyObject *args)
{
	unsigned long x1, x2;

	if (!PyArg_ParseTuple(args, "i:htons", &x1)) {
		return NULL;
	}
	x2 = (int)htons((short)x1);
	return PyInt_FromLong(x2);
}
