// Copyright © 2009 Red Hat, Inc.
//
// This software is licensed to you under the GNU Lesser General Public
// License, version 2.1 (LGPLv2.1). There is NO WARRANTY for this software,
// express or implied, including the implied warranties of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. You should have received a copy of
// LGPLv2.1 along with this software; if not, see
// http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt
//
// Red Hat trademarks are not licensed under LGPLv2.1. No permission is
// granted to use or replicate Red Hat trademarks that are incorporated in
// this software or its documentation.
// 
// Red Hat Author(s): David Hugh Malcolm <dmalcolm@redhat.com>

@initialize:python@
"""
Analyze format strings passed to variadic function, compare to vararg types actually passed
"""
import sys
sys.path.append('.')
from validate import validate_type
num_errors = 0

@ check_PyArg_ParseTuple @
position pos;
expression args;
expression fmt;
expression list[len_E] E;
expression list[len_F] F;
type t;
t e;
@@

PyArg_ParseTuple@pos(args, fmt, E, e, F)

@script:python@
pos << check_PyArg_ParseTuple.pos;
fmt << check_PyArg_ParseTuple.fmt;
len_E << check_PyArg_ParseTuple.len_E;
len_F << check_PyArg_ParseTuple.len_F;
t << check_PyArg_ParseTuple.t;
@@
num_errors += validate_type(pos[0], 
    fmt.expr,
    int(len_E), 
    int(len_E) + 1 + int(len_F),
    t)



# likewise for PyArg_Parse etc:

@ check_PyArg_Parse @
position pos;
expression args;
expression fmt;
expression list[len_E] E;
expression list[len_F] F;
type t;
t e;
@@

PyArg_Parse@pos(args, fmt, E, e, F)

@script:python@
pos << check_PyArg_Parse.pos;
fmt << check_PyArg_Parse.fmt;
len_E << check_PyArg_Parse.len_E;
len_F << check_PyArg_Parse.len_F;
t << check_PyArg_Parse.t;
@@
num_errors += validate_type(pos[0], 
    fmt.expr,
    int(len_E), 
    int(len_E) + 1 + int(len_F),
    t)



@ check_PyArg_ParseTupleAndKeywords @
position pos;
expression args;
expression keywords;
expression fmt;
expression argnames;
expression list[len_E] E;
expression list[len_F] F;
type t;
t e;
@@

PyArg_ParseTupleAndKeywords@pos(args, keywords, fmt, argnames, E, e, F)

@script:python@
pos << check_PyArg_ParseTupleAndKeywords.pos;
fmt << check_PyArg_ParseTupleAndKeywords.fmt;
len_E << check_PyArg_ParseTupleAndKeywords.len_E;
len_F << check_PyArg_ParseTupleAndKeywords.len_F;
t << check_PyArg_ParseTupleAndKeywords.t;
@@
num_errors += validate_type(pos[0], 
    fmt.expr,
    int(len_E), 
    int(len_E) + 1 + int(len_F),
    t)




@script:python @
@@
# Shutdown hook: report the number of errors for use in Makefiles etc:
sys.exit(num_errors)
